the_count = ['brown', 'blond', 'red']
eyes = ['brown', 'blue', 'green']
weights = [1, 2, 3, 4]

for i in the_count:
    print(i)

for eye in eyes:
    print(f"eye color is {eye}")


num_pies = 50
print(f"I can eat {num_pies} pies")

sum = 0
for weight in weights:
    sum += weight

print(sum)

print("Another change to the file without pies")

